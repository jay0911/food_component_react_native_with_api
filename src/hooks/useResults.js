import React,{useState,useEffect} from 'react';
import yelp from '../api/yelp'

export default () =>{
    const [results,setResults] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');

    const searchApi = async (searchTerm) => {
        try{
            const response = await yelp.get('/search',{
                params:{
                    limit:50,
                    term: searchTerm,
                    location: 'san jose'
                }
            });
            setResults(response.data.businesses)
            setErrorMessage('');
        } catch (err){
           setErrorMessage('Something went wrong');
        }    
    }

    //run code 1 time or many time in array
    useEffect(()=>{
        searchApi('pasta');
    },[])

    return [searchApi,results,errorMessage];
}