import React,{useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView
} from 'react-native';
import SearchBar from '../components/SearchBar'
import useResults from '../hooks/useResults'
import ResultsList from '../components/ResultsList'

const SearchScreen = () => {
    const [term,setTerm] = useState('');
    const [searchApi,results,errorMessage] = useResults();

    const filterResultByPrice = (price) =>{
        return results.filter( result =>{
            return result.price === price;
        });
    }

    return (
    <View style={{flex:1}}>
        <SearchBar 
        term={term} 
        onTermChange={(newTerm)=>setTerm(newTerm)}
        onTermSubmit={() =>searchApi(term)} />
        {errorMessage?<Text>{errorMessage}</Text>:null}
        <Text style={styles.searchFoundCount}>We have found {results.length}</Text>
        <ScrollView>
            <ResultsList  
            results={filterResultByPrice('$')} 
            title="Cost Effective"/>
            
            <ResultsList 
            results={filterResultByPrice('$$')} 
            title="Bit Pricier"/>
            
            <ResultsList 
            results={filterResultByPrice('$$$')} 
            title="Big Spender"/>
            
        </ScrollView>
    </View>
    )
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize:30
    },
    searchFoundCount:{
        marginLeft:15,
        marginBottom: 10
    }
})

export default SearchScreen;