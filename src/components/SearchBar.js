import React from 'react';
import {
  StyleSheet,
  TextInput,
  View
} from 'react-native';
import Icon from 'react-native-ionicons'
 
const SearchBar = ({term,onTermChange,onTermSubmit}) => {
    return (
    <View style={styles.backgroundStyle}>
        <Icon name="search" style={styles.iconStyle} />
        <TextInput
        onEndEditing={onTermSubmit}
        autoCapitalize='none'
        autoCorrect={false}
        style={styles.inputStyle} 
        placeholder="Search"
        value={term}
        onChangeText={onTermChange} />
    </View>
    )
};

const styles = StyleSheet.create({
    backgroundStyle: {
        backgroundColor:'#dde9ee',
        marginTop: 15,
        height:60,
        borderRadius:5,
        marginHorizontal: 15,
        flexDirection: 'row',
        marginBottom: 10
    },
    inputStyle:{
        borderColor: 'black',
        flex:1,
        fontSize: 18
    },
    iconStyle:{
        fontSize:35,
        alignSelf:'center',
        marginHorizontal:15
    }
})

export default SearchBar;